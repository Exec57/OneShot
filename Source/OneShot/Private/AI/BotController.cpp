// Fill out your copyright notice in the Description page of Project Settings.

#include "BotController.h"
#include "BotCharacter.h"
#include "SGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "SPlayerState.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"

ABotController::ABotController()
{
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));

	bWantsPlayerState = true;

	PrimaryActorTick.bCanEverTick = true;
}

void ABotController::BeginPlay()
{
	Super::BeginPlay();

	ASPlayerState* SPlayerState = Cast<ASPlayerState>(PlayerState);
	if (SPlayerState)
	{
		SPlayerState->bIsABot = true;
		SPlayerState->SetPlayerName(TEXT("Bot"));
		SPlayerState->PlayerColor = FColor::MakeRandomColor();
	}
}

void ABotController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (BlackboardComp)
	{
		APawn* Player = Cast<APawn>(BlackboardComp->GetValueAsObject("Player"));
		if (Player)
		{
			//SetFocalPoint(Player->GetActorLocation());

			//SetFocus(Player);
		}
	}
}

void ABotController::SpawnBot()
{
	if (BotCharacterClass == nullptr)
	{
		return;
	}

	AActor* PlayerStart = GetWorld()->GetAuthGameMode()->ChoosePlayerStart(this);

	FTransform Transform;
	Transform.SetLocation(PlayerStart->GetActorLocation());
	Transform.SetRotation(PlayerStart->GetActorRotation().Quaternion());
	ABotCharacter* Bot = GetWorld()->SpawnActorDeferred<ABotCharacter>(BotCharacterClass, Transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if (Bot)
	{
		Possess(Bot);
	}
	UGameplayStatics::FinishSpawningActor(Bot, Transform);
}

void ABotController::OnDeath()
{
	GetWorldTimerManager().SetTimer(SpawnTimer, this, &ABotController::SpawnBot, 3.0f);
}

void ABotController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	ABotCharacter* BotCharacter = Cast<ABotCharacter>(InPawn);
	if (BotCharacter)
	{
		if (BotCharacter->GetBehaviorTree()->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*BotCharacter->GetBehaviorTree()->BlackboardAsset);
		}

		BehaviorComp->StartTree(*BotCharacter->GetBehaviorTree());
	}
}

void ABotController::UnPossess()
{
	Super::UnPossess();

	BehaviorComp->StopTree();
}

void ABotController::SetBlackboardPlayer(APawn* Player)
{
	BlackboardComp->SetValueAsObject("Player", Player);
}
