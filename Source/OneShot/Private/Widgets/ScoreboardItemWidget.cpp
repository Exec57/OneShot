// Fill out your copyright notice in the Description page of Project Settings.

#include "ScoreboardItemWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"

void UScoreboardItemWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Image_PlayerColor = Cast<UImage>(WidgetTree->FindWidget("Image_PlayerColor"));
	TextBlock_Pseudo = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_Pseudo"));
	TextBlock_Score = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_Score"));
	TextBlock_Kills = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_Kills"));
	TextBlock_Deaths = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_Deaths"));
	TextBlock_Ping = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_Ping"));
}

void UScoreboardItemWidget::SetPlayerColor(FColor Color)
{
	Image_PlayerColor->SetColorAndOpacity(Color);
}

void UScoreboardItemWidget::SetPseudo(FString Name)
{
	TextBlock_Pseudo->SetText(FText::FromString(Name));
}

void UScoreboardItemWidget::SetScore(float Value)
{
	TextBlock_Score->SetText(FText::AsNumber(Value));
}

void UScoreboardItemWidget::SetKills(uint8 Value)
{
	TextBlock_Kills->SetText(FText::AsNumber(Value));
}

void UScoreboardItemWidget::SetDeaths(uint8 Value)
{
	TextBlock_Deaths->SetText(FText::AsNumber(Value));
}

void UScoreboardItemWidget::SetPing(uint8 Value)
{
	TextBlock_Ping->SetText(FText::AsNumber(Value));
}
