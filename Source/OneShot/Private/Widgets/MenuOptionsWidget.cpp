// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuOptionsWidget.h"
#include "Components/Button.h"
#include "Blueprint/WidgetTree.h"
#include "SPlayerController.h"
#include "SHUD.h"
#include "Components/Slider.h"
#include "Kismet/GameplayStatics.h"
#include "SSaveGame.h"
#include "Components/TextBlock.h"
#include "Components/ComboBoxString.h"
#include "HumanCharacter.h"

void UMenuOptionsWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Button_Back = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_Back")));
	Slider_Volume = Cast<USlider>(WidgetTree->FindWidget(TEXT("Slider_Volume")));
	TextBlock_Volume = Cast<UTextBlock>(WidgetTree->FindWidget(TEXT("TextBlock_Volume")));
	Slider_MouseSpeed = Cast<USlider>(WidgetTree->FindWidget(TEXT("Slider_MouseSpeed")));
	TextBlock_MouseSpeed = Cast<UTextBlock>(WidgetTree->FindWidget(TEXT("TextBlock_MouseSpeed")));
	ComboBoxString_KeyboardMode = Cast<UComboBoxString>(WidgetTree->FindWidget(TEXT("ComboBoxString_KeyboardMode")));

	SaveGame_Options = USSaveGame::GetInstance("Options");

	Slider_Volume->SetValue(SaveGame_Options->Volume);
	TextBlock_Volume->SetText(FText::AsPercent(SaveGame_Options->Volume));
	Slider_MouseSpeed->SetValue(SaveGame_Options->MouseSpeed / 2.0f);
	TextBlock_MouseSpeed->SetText(FText::AsNumber(SaveGame_Options->MouseSpeed));
	ComboBoxString_KeyboardMode->SetSelectedOption(SaveGame_Options->KeyboardMode);

	Button_Back->OnClicked.AddDynamic(this, &UMenuOptionsWidget::OnButtonClicked_Back);
	Slider_Volume->OnValueChanged.AddDynamic(this, &UMenuOptionsWidget::OnSliderValueChanged_Volume);
	Slider_MouseSpeed->OnValueChanged.AddDynamic(this, &UMenuOptionsWidget::OnSliderValueChanged_MouseSpeed);
}

void UMenuOptionsWidget::NativeDestruct()
{
	Super::NativeDestruct();

	Button_Back->OnClicked.Clear();
	Slider_Volume->OnValueChanged.Clear();
	Slider_MouseSpeed->OnValueChanged.Clear();

	SaveGame_Options->KeyboardMode = ComboBoxString_KeyboardMode->GetSelectedOption();

	SaveGame_Options->Save();

	ASPlayerController* PlayerController = Cast<ASPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->UpdateVolume();

		AHumanCharacter* Character = Cast<AHumanCharacter>(PlayerController->GetPawn());
		if (Character)
		{
			Character->UpdateSettings();
		}
	}
}

void UMenuOptionsWidget::OnButtonClicked_Back()
{
	RemoveFromParent();
}

void UMenuOptionsWidget::OnSliderValueChanged_Volume(float Value)
{
	TextBlock_Volume->SetText(FText::AsPercent(SaveGame_Options->Volume));
	SaveGame_Options->Volume = Value;
}

void UMenuOptionsWidget::OnSliderValueChanged_MouseSpeed(float Value)
{
	float Speed = FMath::Lerp(0.1f, 2.0f, Value);
	Speed = FMath::FloorToFloat(Speed * 10.0f) / 10.0f;
	TextBlock_MouseSpeed->SetText(FText::AsNumber(Speed));
	SaveGame_Options->MouseSpeed = Speed;
}