// Fill out your copyright notice in the Description page of Project Settings.

#include "ScreenPlayWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Blueprint/WidgetTree.h"
#include "Components/ProgressBar.h"
#include "Components/VerticalBox.h"
#include "InfoItemWidget.h"
#include "SPlayerState.h"
#include "InfoItemWidget.h"
#include "Components/VerticalBoxSlot.h"

void UScreenPlayWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Image_Crosshair = Cast<UImage>(WidgetTree->FindWidget(TEXT("Image_Crosshair")));
	TextBlock_Plus = Cast<UTextBlock>(WidgetTree->FindWidget(TEXT("TextBlock_Plus")));
	Image_Death = Cast<UImage>(WidgetTree->FindWidget(TEXT("Image_Death")));
	ProgressBar_Reload = Cast<UProgressBar>(WidgetTree->FindWidget(TEXT("ProgressBar_Reload")));
	VerticalBox_Info = Cast<UVerticalBox>(WidgetTree->FindWidget(TEXT("VerticalBox_Info")));
}

void UScreenPlayWidget::NativeDestruct()
{
	Super::NativeDestruct();

	
}

void UScreenPlayWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (ProgressBar_Reload->GetVisibility() == ESlateVisibility::Visible)
	{
		float Percent = ProgressBar_Reload->Percent + InDeltaTime / FireRate;
		ProgressBar_Reload->SetPercent(Percent);

		if (Percent >= 1.0f)
		{
			ProgressBar_Reload->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

void UScreenPlayWidget::OnPlayerSpawn()
{
	Image_Crosshair->SetVisibility(ESlateVisibility::Visible);
	TextBlock_Plus->SetVisibility(ESlateVisibility::Hidden);
	Image_Death->SetVisibility(ESlateVisibility::Hidden);
	ProgressBar_Reload->SetVisibility(ESlateVisibility::Hidden);
}

void UScreenPlayWidget::OnPlayerDeath()
{
	Image_Crosshair->SetVisibility(ESlateVisibility::Hidden);
	Image_Death->SetVisibility(ESlateVisibility::Visible);
}

void UScreenPlayWidget::OnPlayerKill(bool bFireInHead)
{
	if (bFireInHead)
	{
		TextBlock_Plus->SetText(FText::FromString(TEXT("+2")));
	}
	else
	{
		TextBlock_Plus->SetText(FText::FromString(TEXT("+1")));
	}

	TextBlock_Plus->SetVisibility(ESlateVisibility::Visible);

	PlayPlusAnimation();
}

void UScreenPlayWidget::OnPlayerFire(float FireRate)
{
	ProgressBar_Reload->SetVisibility(ESlateVisibility::Visible);
	ProgressBar_Reload->SetPercent(0.0f);
	this->FireRate = FireRate;
}

UInfoItemWidget* UScreenPlayWidget::CreateAndAddInfoItem()
{
	UInfoItemWidget* Item = CreateWidget<UInfoItemWidget>(GetOwningPlayer(), InfoItemWidgetClass);

	TArray<UWidget*> ItemArray;

	for (int32 i = 0; i < VerticalBox_Info->GetChildrenCount(); ++i)
	{
		ItemArray.Add(VerticalBox_Info->GetChildAt(i));
	}

	VerticalBox_Info->ClearChildren();

	if (Item)
	{
		UVerticalBoxSlot* Slot = VerticalBox_Info->AddChildToVerticalBox(Item);
		Slot->SetHorizontalAlignment(HAlign_Right);

		for (int32 i = 0; i < ItemArray.Num(); ++i)
		{
			UVerticalBoxSlot* SlotChild = VerticalBox_Info->AddChildToVerticalBox(ItemArray[i]);
			SlotChild->SetHorizontalAlignment(HAlign_Right);
		}
	}

	return Item;
}

void UScreenPlayWidget::NewShotInfo(ASPlayerState* Killer, ASPlayerState* Target)
{
	UInfoItemWidget* Item = CreateAndAddInfoItem();
	if (Item)
	{
		Item->SetTextOneInfo(Killer->GetPlayerName(), Killer->PlayerColor);
		Item->SetTextTwoInfo(" shot ");
		Item->SetTextThreeInfo(Target->GetPlayerName(), Target->PlayerColor);
	}
}

void UScreenPlayWidget::NewTextInfo(const FString& TextOne, FColor Color, const FString& TextTwo)
{
	UInfoItemWidget* Item = CreateAndAddInfoItem();
	if (Item)
	{
		Item->SetTextTwoInfo(TextOne, Color);
		Item->SetTextThreeInfo(TextTwo);
	}
}
