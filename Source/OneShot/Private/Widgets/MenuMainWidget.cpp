// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuMainWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Kismet/GameplayStatics.h"
#include "SSaveGame.h"
#include "SHUD.h"

void UMenuMainWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Button_Solo = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_Solo")));
	Button_ListenServer = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_ListenServer")));
	Button_JoinServer = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_JoinServer")));
	TextBox_IP = Cast<UEditableTextBox>(WidgetTree->FindWidget(TEXT("TextBox_IP")));
	Button_Options = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_Options")));
	Button_PlayerSettings = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_PlayerSettings")));
	Button_Exit = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_Exit")));

	SaveGame_IP = USSaveGame::GetInstance("IP");
	TextBox_IP->SetText(SaveGame_IP->IpAddress);

	Button_Solo->OnClicked.AddDynamic(this, &UMenuMainWidget::OnButtonClicked_Solo);
	Button_ListenServer->OnClicked.AddDynamic(this, &UMenuMainWidget::OnButtonClicked_ListenServer);
	Button_JoinServer->OnClicked.AddDynamic(this, &UMenuMainWidget::OnButtonClicked_JoinServer);
	Button_Options->OnClicked.AddDynamic(this, &UMenuMainWidget::OnButtonClicked_Options);
	Button_PlayerSettings->OnClicked.AddDynamic(this, &UMenuMainWidget::OnButtonClicked_PlayerSettings);
	Button_Exit->OnClicked.AddDynamic(this, &UMenuMainWidget::OnButtonClicked_Exit);
}

void UMenuMainWidget::NativeDestruct()
{
	Super::NativeDestruct();

	Button_Solo->OnClicked.Clear();
	Button_ListenServer->OnClicked.Clear();
	Button_JoinServer->OnClicked.Clear();
	Button_Options->OnClicked.Clear();
	Button_PlayerSettings->OnClicked.Clear();
	Button_Exit->OnClicked.Clear();
}

void UMenuMainWidget::OnButtonClicked_Solo()
{
	SaveSettings();

	UGameplayStatics::OpenLevel(GetWorld(), "Test");
}

void UMenuMainWidget::OnButtonClicked_ListenServer()
{
	SaveSettings();

	UGameplayStatics::OpenLevel(GetWorld(), "Test", true, "?listen");
}

void UMenuMainWidget::OnButtonClicked_JoinServer()
{
	SaveSettings();

	FName Name = FName(*TextBox_IP->GetText().ToString());
	UGameplayStatics::OpenLevel(GetWorld(), Name);
}

void UMenuMainWidget::OnButtonClicked_Options()
{
	SaveSettings();

	APlayerController* PlayerController = GetOwningPlayer();
	if (PlayerController)
	{
		ASHUD* HUD = Cast<ASHUD>(PlayerController->GetHUD());
		if (HUD)
		{	
			HUD->SpawnMenuOptions();
		}
	}
}

void UMenuMainWidget::OnButtonClicked_PlayerSettings()
{
	APlayerController* PlayerController = GetOwningPlayer();
	if (PlayerController)
	{
		ASHUD* HUD = Cast<ASHUD>(PlayerController->GetHUD());
		if (HUD)
		{
			HUD->SpawnMenuPlayerSettings();
		}
	}
}

void UMenuMainWidget::OnButtonClicked_Exit()
{
	SaveSettings();

	GetOwningPlayer()->ConsoleCommand("quit");
}

void UMenuMainWidget::SaveSettings()
{
	SaveGame_IP->IpAddress = TextBox_IP->GetText();
	SaveGame_IP->Save();
}
