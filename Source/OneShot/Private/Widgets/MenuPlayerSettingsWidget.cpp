// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuPlayerSettingsWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/EditableTextBox.h"
#include "Components/Image.h"
#include "Components/Button.h"
#include "SSaveGame.h"

void UMenuPlayerSettingsWidget::NativeConstruct()
{
	Super::NativeConstruct();

	TextBox_Pseudo = Cast<UEditableTextBox>(WidgetTree->FindWidget("TextBox_Pseudo"));
	TextBox_Red = Cast<UEditableTextBox>(WidgetTree->FindWidget("TextBox_Red"));
	TextBox_Green = Cast<UEditableTextBox>(WidgetTree->FindWidget("TextBox_Green"));
	TextBox_Blue = Cast<UEditableTextBox>(WidgetTree->FindWidget("TextBox_Blue"));
	Image_Color = Cast<UImage>(WidgetTree->FindWidget("Image_Color"));
	Button_Apply = Cast<UButton>(WidgetTree->FindWidget("Button_Apply"));
	Button_Back = Cast<UButton>(WidgetTree->FindWidget("Button_Back"));

	SaveGame_PlayerSettings = USSaveGame::GetInstance("PlayerSettings");

	TextBox_Pseudo->SetText(FText::FromString(SaveGame_PlayerSettings->PlayerName));
	TextBox_Red->SetText(FText::AsNumber(SaveGame_PlayerSettings->PlayerColor.R));
	TextBox_Green->SetText(FText::AsNumber(SaveGame_PlayerSettings->PlayerColor.G));
	TextBox_Blue->SetText(FText::AsNumber(SaveGame_PlayerSettings->PlayerColor.B));
	Image_Color->SetColorAndOpacity(SaveGame_PlayerSettings->PlayerColor);

	TextBox_Red->OnTextChanged.AddDynamic(this, &UMenuPlayerSettingsWidget::OnTextBoxTextChanged_Red);
	TextBox_Green->OnTextChanged.AddDynamic(this, &UMenuPlayerSettingsWidget::OnTextBoxTextChanged_Green);
	TextBox_Blue->OnTextChanged.AddDynamic(this, &UMenuPlayerSettingsWidget::OnTextBoxTextChanged_Blue);
	Button_Apply->OnClicked.AddDynamic(this, &UMenuPlayerSettingsWidget::OnButtonClicked_Apply);
	Button_Back->OnClicked.AddDynamic(this, &UMenuPlayerSettingsWidget::OnButtonClicked_Back);
}

void UMenuPlayerSettingsWidget::NativeDestruct()
{
	Super::NativeDestruct();

	TextBox_Red->OnTextChanged.Clear();
	TextBox_Green->OnTextChanged.Clear();
	TextBox_Blue->OnTextChanged.Clear();
	Button_Apply->OnClicked.Clear();
	Button_Back->OnClicked.Clear();
}

void UMenuPlayerSettingsWidget::OnTextBoxTextChanged_Red(const FText& Text)
{
	float Value = FCString::Atof(*Text.ToString());
	SaveGame_PlayerSettings->PlayerColor.R = Value;
	Image_Color->SetColorAndOpacity(SaveGame_PlayerSettings->PlayerColor);
}

void UMenuPlayerSettingsWidget::OnTextBoxTextChanged_Green(const FText& Text)
{
	float Value = FCString::Atof(*Text.ToString());
	SaveGame_PlayerSettings->PlayerColor.G = Value;
	Image_Color->SetColorAndOpacity(SaveGame_PlayerSettings->PlayerColor);
}

void UMenuPlayerSettingsWidget::OnTextBoxTextChanged_Blue(const FText& Text)
{
	float Value = FCString::Atof(*Text.ToString());
	SaveGame_PlayerSettings->PlayerColor.B = Value;
	Image_Color->SetColorAndOpacity(SaveGame_PlayerSettings->PlayerColor);
}

void UMenuPlayerSettingsWidget::OnButtonClicked_Apply()
{
	SaveGame_PlayerSettings->PlayerName = TextBox_Pseudo->GetText().ToString();
	SaveGame_PlayerSettings->Save();

	RemoveFromParent();
}

void UMenuPlayerSettingsWidget::OnButtonClicked_Back()
{
	RemoveFromParent();
}

