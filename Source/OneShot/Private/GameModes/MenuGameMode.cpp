// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuGameMode.h"
#include "SHUD.h"
#include "SSaveGame.h"

void AMenuGameMode::StartPlay()
{
	Super::StartPlay();

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController)
	{
		FInputModeGameAndUI Mode;
		Mode.SetHideCursorDuringCapture(false);
		PlayerController->bShowMouseCursor = true;
		PlayerController->SetInputMode(Mode);

		ASHUD* HUD = Cast<ASHUD>(PlayerController->GetHUD());
		if (HUD)
		{
			HUD->SpawnMenuMain();
			if (!USSaveGame::Exist("PlayerSettings"))
			{
				HUD->SpawnMenuPlayerSettings();
			}
		}
	}
}
