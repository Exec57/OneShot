// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameMode.h"
#include "BaseCharacter.h"
#include "SHUD.h"
#include "SPlayerController.h"
#include "SGameState.h"
#include "SPlayerState.h"
#include "BotController.h"

ASGameMode::ASGameMode()
{
	DefaultPawnClass = ABaseCharacter::StaticClass();
	HUDClass = ASHUD::StaticClass();
	PlayerControllerClass = ASPlayerController::StaticClass();
	GameStateClass = ASGameState::StaticClass();
	PlayerStateClass = ASPlayerState::StaticClass();

	bStartPlayersAsSpectators = true;

	NumBot = 5;
}

void ASGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	ASPlayerController* PlayerController = Cast<ASPlayerController>(NewPlayer);
	if (PlayerController)
	{
		PlayerController->ClientOnLogin();
	}
}

void ASGameMode::Logout(AController* Exiting)
{
	ASGameState* SGameState = Cast<ASGameState>(GameState);
	if (SGameState)
	{
		ASPlayerState* SPlayerState = Cast<ASPlayerState>(Exiting->PlayerState);
		if (SPlayerState)
		{
			SGameState->MulticastOnPlayerDisconnect(SPlayerState->GetPlayerName(), SPlayerState->PlayerColor);
		}
	}

	Super::Logout(Exiting);
}

void ASGameMode::StartPlay()
{
	Super::StartPlay();

	for (int32 i = 0; i < NumBot; i++)
	{
		SpawnBotController();
	}
}

void ASGameMode::SpawnBotController()
{
	if (BotControllerClass)
	{
		FActorSpawnParameters SpawnParams;
		ABotController* BotController = GetWorld()->SpawnActor<ABotController>(BotControllerClass, SpawnParams);
		BotController->SpawnBot();
	}
}
