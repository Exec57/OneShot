// Fill out your copyright notice in the Description page of Project Settings.

#include "SPlayerController.h"
#include "BaseCharacter.h"
#include "SHUD.h"
#include "SGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "SSaveGame.h"
#include "SPlayerState.h"
#include "HumanCharacter.h"
#include "SGameState.h"

void ASPlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SPlayerState = Cast<ASPlayerState>(PlayerState);

	SGameState = Cast<ASGameState>(GetWorld()->GetGameState());
}

void ASPlayerController::BeginPlay()
{
	Super::BeginPlay();

	UpdateVolume();
}

void ASPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Escape", IE_Pressed, this, &ASPlayerController::OnEscapeKey);

	InputComponent->BindAction("Scoreboard", IE_Pressed, this, &ASPlayerController::ShowScoreboard);
	InputComponent->BindAction("Scoreboard", IE_Released, this, &ASPlayerController::HideScoreboard);
}

void ASPlayerController::Death()
{
	GetWorldTimerManager().SetTimer(RespawnTimer, this, &ASPlayerController::SpawnPlayer, 3);

	ClientOnDeath();
}

void ASPlayerController::ClientOnDeath_Implementation()
{
	if (SHUD)
	{
		SHUD->OnPlayerDeath();
	}
	
	ClientPlayCameraShake(CameraShakeClass);

	UGameplayStatics::SpawnSound2D(GetWorld(), DeathSound);
}

void ASPlayerController::Possess(APawn* aPawn)
{
	Super::Possess(aPawn);

	ClientOnPossess();
}

void ASPlayerController::ClientOnPossess_Implementation()
{
	if (SHUD)
	{
		SHUD->OnPlayerSpawn();
	}
}

void ASPlayerController::SpawnPlayer()
{
	if (CharacterClass == nullptr)
	{
		return;
	}

	AActor* PlayerStart = GetWorld()->GetAuthGameMode()->ChoosePlayerStart(this);

	FTransform Transform;
	Transform.SetLocation(PlayerStart->GetActorLocation());
	Transform.SetRotation(PlayerStart->GetActorRotation().Quaternion());
	AHumanCharacter* Character = GetWorld()->SpawnActorDeferred<AHumanCharacter>(CharacterClass, Transform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if (Character)
	{
		Possess(Character);
	}
	UGameplayStatics::FinishSpawningActor(Character, Transform);
}

void ASPlayerController::ShowScoreboard()
{
	if (bPause)
	{
		return;
	}

	if (SHUD)
	{
		SHUD->ShowScoreboard();
	}
}

void ASPlayerController::HideScoreboard()
{
	if (SHUD)
	{
		SHUD->HideScoreboard();
	}
}

void ASPlayerController::SetFocus(bool NewState)
{
	bShowMouseCursor = NewState;

	if (NewState)
	{
		FInputModeGameAndUI Mode;
		Mode.SetHideCursorDuringCapture(false);
		SetInputMode(Mode);
		if (GetPawn())
		{
			GetPawn()->DisableInput(this);
		}
	}
	else
	{
		SetInputMode(FInputModeGameOnly());
		ABaseCharacter* BaseCharacter = Cast<ABaseCharacter>(GetCharacter());
		if (BaseCharacter && !BaseCharacter->IsDead())
		{
			GetPawn()->EnableInput(this);
		}
	}
}

void ASPlayerController::TogglePause()
{
	if (SHUD)
	{
		bPause = !bPause;
		SHUD->ToggleMenuPause();
		UpdateFocus();
	}
}

void ASPlayerController::UpdateVolume()
{
	USSaveGame* SaveGame_Options = USSaveGame::GetInstance("Options");
	UGameplayStatics::SetSoundMixClassOverride(GetWorld(), SoundMix, SoundClass, SaveGame_Options->Volume);
}

bool ASPlayerController::ServerSpawn_Validate(const FString& PlayerName, FColor PlayerColor)
{
	return true;
}

void ASPlayerController::ServerSpawn_Implementation(const FString& PlayerName, FColor PlayerColor)
{
	if (SPlayerState)
	{
		SPlayerState->SetPlayerName(PlayerName);
		SPlayerState->PlayerColor = PlayerColor;
	}

	SpawnPlayer();

	if (SGameState)
	{
		SGameState->MulticastOnPlayerConnect(this, SPlayerState->GetPlayerName(), SPlayerState->PlayerColor);
	}
}

void ASPlayerController::ClientOnLogin_Implementation()
{
	SHUD = Cast<ASHUD>(GetHUD());
	if (SHUD)
	{
		SHUD->SpawnScreenPlay();
	}

	USSaveGame* SaveGame_PlayerSettings = USSaveGame::GetInstance("PlayerSettings");

	ServerSpawn(SaveGame_PlayerSettings->PlayerName, SaveGame_PlayerSettings->PlayerColor);
}

void ASPlayerController::OnEscapeKey()
{
	TogglePause();
}

void ASPlayerController::UpdateFocus()
{
	if (bPause)
	{
		SetFocus(true);
	}
	else
	{
		if (GetPawn() == nullptr)
		{
			SetFocus(true);
		}
		else
		{
			SetFocus(false);
		}
	}
}

void ASPlayerController::ClientOnKill_Implementation(bool bFireInHead)
{
	UGameplayStatics::SpawnSound2D(GetWorld(), KillSound);

	if (SHUD)
	{
		SHUD->OnPlayerKill(bFireInHead);
	}
	
}
