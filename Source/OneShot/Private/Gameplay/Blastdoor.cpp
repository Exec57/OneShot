// Fill out your copyright notice in the Description page of Project Settings.

#include "Blastdoor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"

ABlastdoor::ABlastdoor()
{
	PrimaryActorTick.bCanEverTick = true;

	BlastdoorBorderMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlastdoorBorder"));
	RootComponent = BoxComp;

	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	BoxComp->SetupAttachment(BlastdoorBorderMeshComp);
	BoxComp->SetRelativeLocation(FVector(-80.0f, -170.0f, 170.0f));
	BoxComp->SetBoxExtent(FVector(300.0f, 170.0f, 170.0f));

	BlastdoorMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Blastdoor"));
	BlastdoorMeshComp->SetupAttachment(BlastdoorBorderMeshComp);
	BlastdoorMeshComp->SetRelativeLocation(FVector(-80.0f, 0.0f, 0.0f));

	TargetZLocation = 0.0f;
	ZLocation = 0.0f;
	Count = 0;
	bOpen = false;
}

void ABlastdoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ZLocation = FMath::FInterpConstantTo(ZLocation, TargetZLocation, DeltaTime, 2000.0f);
	BlastdoorMeshComp->SetRelativeLocation(FVector(-80.0f, 0.0f, ZLocation));
}

void ABlastdoor::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	Count++;

	if (GetWorldTimerManager().IsTimerActive(CloseTimer))
	{
		GetWorldTimerManager().ClearTimer(CloseTimer);
	}
	else if (!bOpen)
	{
		Open();
	}
}

void ABlastdoor::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	Count--;

	if (Count == 0)
	{
		GetWorldTimerManager().SetTimer(CloseTimer, this, &ABlastdoor::Close, 3.0f);
	}
}

void ABlastdoor::Open()
{
	TargetZLocation = 340.0f;

	UGameplayStatics::SpawnSoundAttached(Sound, BoxComp);

	bOpen = true;
}

void ABlastdoor::Close()
{
	TargetZLocation = 0.0f;

	UGameplayStatics::SpawnSoundAttached(Sound, BoxComp);

	bOpen = false;
}