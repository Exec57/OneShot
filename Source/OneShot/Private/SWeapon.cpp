// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "OneShot.h"
#include "Net/UnrealNetwork.h"
#include "BaseCharacter.h"
#include "SPlayerState.h"
#include "SPlayerController.h"
#include "SHUD.h"

ASWeapon::ASWeapon()
{
	SetReplicates(true);

	FireRate = 1.0f;

	LastFireTime = -1;

	bRepeat = false;

	NetUpdateFrequency = 66.0f;
	MinNetUpdateFrequency = 33.0f;

	HeadSocketName = "head";
}

void ASWeapon::BeginPlay()
{
	Super::BeginPlay();

	Character = Cast<ABaseCharacter>(GetOwner());
	if (Character && Character->PlayerState)
	{
		bIsABot = Character->PlayerState->bIsABot;
	}
}

void ASWeapon::StartFire()
{
	float FirstDelay = FMath::Max(LastFireTime + FireRate - GetWorld()->TimeSeconds, 0.0f);

	if (FirstDelay > 0.0f && !bIsABot)
	{
		UGameplayStatics::SpawnSound2D(GetWorld(), CantFireSound);
	}

	if (!bRepeat && FirstDelay > 0.0f)
	{
		return;
	}

	GetWorldTimerManager().SetTimer(FireTimer, this, &ASWeapon::Fire, FireRate, bRepeat, FirstDelay);
}

void ASWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(FireTimer);
}

void ASWeapon::Fire()
{
	FHitResult Hit;

	if (Character)
	{
		FVector EyeLocation;
		FRotator EyeRotation;
		Character->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector TraceEnd = EyeLocation + EyeRotation.Vector() * 10000;

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);
		QueryParams.AddIgnoredActor(Character);
		QueryParams.bTraceComplex = true;
		QueryParams.bReturnPhysicalMaterial = true;

		EPhysicalSurface SurfaceType = EPhysicalSurface::SurfaceType_Default;
		if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))
		{
			TraceEnd = Hit.ImpactPoint;

			SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

			PlayImpactEffect(SurfaceType, TraceEnd, Hit.ImpactNormal.Rotation());
		}

		FVector Location;
		if (!bIsABot)
		{
			Location = EyeLocation - GetActorUpVector() * 10;
		}
		else
		{
			Location = Character->GetMesh()->GetSocketLocation(HeadSocketName);
		}

		PlayTraceEffect(Location, TraceEnd);

		ServerFire(TraceEnd, Hit);

		if (!bIsABot)
		{
			ASPlayerController* PlayerController = Cast<ASPlayerController>(Character->GetController());
			if (PlayerController)
			{
				ASHUD* HUD = Cast<ASHUD>(PlayerController->GetHUD());
				if (HUD)
				{
					HUD->OnPlayerFire(FireRate);
				}
			}

			GetWorldTimerManager().SetTimer(FireRateTimer, this, &ASWeapon::PlayReloadCompleteSound, FireRate);
		}

		LastFireTime = GetWorld()->TimeSeconds;
	}
}

void ASWeapon::ServerFire_Implementation(FVector TraceEnd, FHitResult Hit)
{
	EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

	HitScanTrace.TraceEnd = TraceEnd;
	HitScanTrace.TraceRotation = Hit.ImpactNormal.Rotation();
	HitScanTrace.SurfaceType = SurfaceType;
	HitScanTrace.EnsureReplication();

	if (Character && (SurfaceType == SURFACE_FLESH || SurfaceType == SURFACE_FLESHHEAD))
	{
		ABaseCharacter* Target = Cast<ABaseCharacter>(Hit.Actor);
		if (Target)
		{
			FVector ShotDirection = TraceEnd - Character->GetMesh()->GetSocketLocation(HeadSocketName);
			ShotDirection.Normalize();
			FPointDamageEvent PointDamage(100, Hit, ShotDirection, nullptr);
			Target->TakeDamage(100, PointDamage, Character->GetController(), this);
		}
	}

	if (Character && !Character->IsLocallyControlled())
	{
		OnRep_HitScanTrace();
	}
}

bool ASWeapon::ServerFire_Validate(FVector TraceEnd, FHitResult Hit)
{
	return true;
}

void ASWeapon::OnRep_HitScanTrace()
{
	if (Character)
	{
		FVector Start = Character->GetMesh()->GetSocketLocation(HeadSocketName);
		PlayTraceEffect(Start, HitScanTrace.TraceEnd);
	}

	PlayImpactEffect(HitScanTrace.SurfaceType, HitScanTrace.TraceEnd, HitScanTrace.TraceRotation);
}

void ASWeapon::PlayTraceEffect(FVector Start, FVector End)
{
	if (TrailEffect)
	{
		UParticleSystemComponent* TrailComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TrailEffect, Start);
		if (TrailComp)
		{
			TrailComp->SetBeamEndPoint(0, End);
			if (Character)
			{
				ASPlayerState* SPlayerState = Cast<ASPlayerState>(Character->PlayerState);
				if (SPlayerState)
				{
					FVector Color = FVector(SPlayerState->PlayerColor.R, SPlayerState->PlayerColor.G, SPlayerState->PlayerColor.B);
					TrailComp->SetVectorParameter("Color", Color);
				}
			}
		}
	}

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), FireSound, Start);
}

void ASWeapon::PlayImpactEffect(EPhysicalSurface SurfaceType, FVector ImpactPoint, FRotator TraceRotation)
{
	UParticleSystem* Effect = nullptr;
	switch (SurfaceType)
	{
	case SURFACE_FLESH:
	case SURFACE_FLESHHEAD:
		Effect = BloodEffect;
		break;
	default:
		Effect = ImpactEffect;
		break;
	}

	if (Effect && Character)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Effect, ImpactPoint, TraceRotation);
	}
}

void ASWeapon::PlayReloadCompleteSound()
{
	UGameplayStatics::SpawnSound2D(GetWorld(), ReloadCompleteSound);
}

void ASWeapon::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASWeapon, HitScanTrace, COND_SkipOwner);
}