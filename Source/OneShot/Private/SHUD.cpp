// Fill out your copyright notice in the Description page of Project Settings.

#include "SHUD.h"
#include "Blueprint/UserWidget.h"
#include "MenuPlayerSettingsWidget.h"
#include "MenuMainWidget.h"
#include "MenuOptionsWidget.h"
#include "MenuPauseWidget.h"
#include "ScreenPlayWidget.h"
#include "ScoreboardWidget.h"

void ASHUD::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	
	MenuPlayerSettingsWidget = CreateWidget<UMenuPlayerSettingsWidget>(GetWorld(), MenuPlayerSettingsWidgetClass);
	MenuMainWidget = CreateWidget<UMenuMainWidget>(GetWorld(), MenuMainWidgetClass);
	MenuOptionsWidget = CreateWidget<UMenuOptionsWidget>(GetWorld(), MenuOptionsWidgetClass);
	MenuPauseWidget = CreateWidget<UMenuPauseWidget>(GetWorld(), MenuPauseWidgetClass);
	ScreenPlayWidget = CreateWidget<UScreenPlayWidget>(GetWorld(), ScreenPlayWidgetClass);
	ScoreboardWidget = CreateWidget<UScoreboardWidget>(GetWorld(), ScoreboardWidgetClass);
}

void ASHUD::OnPlayerSpawn()
{
	if (ScreenPlayWidget)
	{
		ScreenPlayWidget->OnPlayerSpawn();
	}
}

void ASHUD::OnPlayerDeath()
{
	ScreenPlayWidget->OnPlayerDeath();
}

void ASHUD::OnPlayerKill(bool bFireInHead)
{
	ScreenPlayWidget->OnPlayerKill(bFireInHead);
}

void ASHUD::OnPlayerFire(float FireRate)
{
	ScreenPlayWidget->OnPlayerFire(FireRate);
}

void ASHUD::SpawnMenuPlayerSettings()
{
	if (MenuPlayerSettingsWidget)
	{
		MenuPlayerSettingsWidget->AddToViewport();
	}
}

void ASHUD::SpawnMenuMain()
{
	if (MenuMainWidget)
	{
		if (MenuOptionsWidget->IsInViewport())
		{
			MenuOptionsWidget->RemoveFromParent();
		}
		MenuMainWidget->AddToViewport();
	}
}

void ASHUD::SpawnMenuOptions()
{
	if (MenuOptionsWidget)
	{
		MenuOptionsWidget->AddToViewport(2);
	}
}

void ASHUD::SpawnScreenPlay()
{
	if (ScreenPlayWidget)
	{
		ScreenPlayWidget->AddToViewport();
	}
}

void ASHUD::ToggleMenuPause()
{
	bool bInPause = true;

	if (MenuPauseWidget->IsInViewport())
	{
		MenuPauseWidget->RemoveFromParent();
		bInPause = false;
	}
	if (MenuOptionsWidget->IsInViewport())
	{
		MenuOptionsWidget->RemoveFromParent();
		bInPause = false;
	}
	if (bInPause)
	{
		MenuPauseWidget->AddToViewport(2);
	}
}

void ASHUD::ShowScoreboard()
{
	if (ScoreboardWidget && !ScoreboardWidget->IsInViewport())
	{
		ScoreboardWidget->AddToViewport(2);
	}
}

void ASHUD::HideScoreboard()
{
	if (ScoreboardWidget && ScoreboardWidget->IsInViewport())
	{
		ScoreboardWidget->RemoveFromParent();
	}
}

void ASHUD::NewShotInfo(ASPlayerState* Killer, ASPlayerState* Target)
{
	ScreenPlayWidget->NewShotInfo(Killer, Target);
}

void ASHUD::NewTextInfo(const FString& TextOne, FColor Color, const FString& TextTwo)
{
	ScreenPlayWidget->NewTextInfo(TextOne, Color, TextTwo);
}
