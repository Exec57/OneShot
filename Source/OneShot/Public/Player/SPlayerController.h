// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SPlayerController.generated.h"

class ABaseCharacter;
class UCameraShake;
class ASHUD;
class ASPlayerState;
class ASGameState;

/**
 * 
 */
UCLASS()
class ONESHOT_API ASPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

public:
	virtual void PostInitializeComponents() override;

	void Death();

	virtual void Possess(APawn* aPawn) override;

	UFUNCTION(Client, Reliable)
	void ClientOnKill(bool bFireInHead);

	void UpdateFocus();

	void SetFocus(bool NewState);

	UFUNCTION(Client, Reliable)
	void ClientOnLogin();

	void TogglePause();

	void UpdateVolume();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSpawn(const FString& PlayerName, FColor PlayerColor);

private:
	ASHUD* SHUD;

	ASPlayerState* SPlayerState;

	ASGameState* SGameState;

	void OnEscapeKey();

	FTimerHandle RespawnTimer;

	void SpawnPlayer();

	UPROPERTY(EditDefaultsOnly, Category = "Character")
	TSubclassOf<ABaseCharacter> CharacterClass;

	UFUNCTION(Client, Reliable)
	void ClientOnDeath();

	UFUNCTION(Client, Reliable)
	void ClientOnPossess();

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundBase* KillSound;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	TSubclassOf<UCameraShake> CameraShakeClass;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundBase* DeathSound;

	bool bPause;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundMix* SoundMix;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundClass* SoundClass;

	void ShowScoreboard();

	void HideScoreboard();
};
