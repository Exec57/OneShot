// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class ONESHOT_API ASPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Replicated)
	FColor PlayerColor;

	UPROPERTY(Replicated)
	uint8 Kills;
	
	UPROPERTY(Replicated)
	uint8 Deaths;
};
