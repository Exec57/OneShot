// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SHUD.generated.h"

class UUserWidget;
class UMenuPlayerSettingsWidget;
class UMenuMainWidget;
class UMenuOptionsWidget;
class UMenuPauseWidget;
class UMenuStartWidget;
class UScreenPlayWidget;
class UScoreboardWidget;
class ASPlayerState;

/**
 * 
 */
UCLASS()
class ONESHOT_API ASHUD : public AHUD
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UMenuPlayerSettingsWidget> MenuPlayerSettingsWidgetClass;
	UPROPERTY()
	UMenuPlayerSettingsWidget* MenuPlayerSettingsWidget;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UMenuMainWidget> MenuMainWidgetClass;
	UPROPERTY()
	UMenuMainWidget* MenuMainWidget;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UMenuOptionsWidget> MenuOptionsWidgetClass;
	UPROPERTY()
	UMenuOptionsWidget* MenuOptionsWidget;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UMenuPauseWidget> MenuPauseWidgetClass;
	UPROPERTY()
	UMenuPauseWidget* MenuPauseWidget;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UScreenPlayWidget> ScreenPlayWidgetClass;
	UPROPERTY()
	UScreenPlayWidget* ScreenPlayWidget;

	UPROPERTY(EditDefaultsOnly, Category = "UI")
	TSubclassOf<UScoreboardWidget> ScoreboardWidgetClass;
	UPROPERTY()
	UScoreboardWidget* ScoreboardWidget;

	FTimerHandle PlusOneTimer;

public:
	virtual void PostInitializeComponents() override;

	void OnPlayerSpawn();

	void OnPlayerDeath();

	void OnPlayerKill(bool bFireInHead);

	void OnPlayerFire(float FireRate);

public:
	void SpawnMenuPlayerSettings();

	void SpawnMenuMain();

	void SpawnMenuOptions();

	void SpawnScreenPlay();

	void ToggleMenuPause();

	void ShowScoreboard();

	void HideScoreboard();

	void NewShotInfo(ASPlayerState* Killer, ASPlayerState* Target);

	void NewTextInfo(const FString& TextOne, FColor Color, const FString& TextTwo);
};
