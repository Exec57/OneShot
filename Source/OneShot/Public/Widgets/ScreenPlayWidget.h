// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScreenPlayWidget.generated.h"

class UImage;
class UTextBlock;
class UProgressBar;
class UVerticalBox;
class UInfoItemWidget;
class ASPlayerState;

/**
 * 
 */
UCLASS()
class ONESHOT_API UScreenPlayWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void OnPlayerSpawn();

	void OnPlayerDeath();

	void OnPlayerKill(bool bFireInHead);

	void OnPlayerFire(float FireRate);

	void NewShotInfo(ASPlayerState* Killer, ASPlayerState* Target);

	void NewTextInfo(const FString& TextOne, FColor Color, const FString& TextTwo);

protected:
	virtual void NativeConstruct() override;

	virtual void NativeDestruct() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
	void PlayPlusAnimation();

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

private:
	UImage* Image_Crosshair;

	UTextBlock* TextBlock_Plus;

	UImage* Image_Death;

	UProgressBar* ProgressBar_Reload;

	UVerticalBox* VerticalBox_Info;

	float FireRate;

	UPROPERTY(EditAnywhere, Category = "UI")
	TSubclassOf<UInfoItemWidget> InfoItemWidgetClass;

	UInfoItemWidget* CreateAndAddInfoItem();
};
