// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuOptionsWidget.generated.h"

class USSaveGame;
class UButton;
class USlider;
class UTextBlock;
class UComboBoxString;

/**
 * 
 */
UCLASS()
class ONESHOT_API UMenuOptionsWidget : public UUserWidget
{
	GENERATED_BODY()
	
private:
	UPROPERTY()
	USSaveGame* SaveGame_Options;

	UButton* Button_Back;

	UFUNCTION()
	void OnButtonClicked_Back();

	USlider* Slider_Volume;

	UFUNCTION()
	void OnSliderValueChanged_Volume(float Value);

	UTextBlock* TextBlock_Volume;

	USlider* Slider_MouseSpeed;

	UFUNCTION()
	void OnSliderValueChanged_MouseSpeed(float Value);

	UTextBlock* TextBlock_MouseSpeed;

	UComboBoxString* ComboBoxString_KeyboardMode;
	
protected:
	virtual void NativeConstruct() override;

	virtual void NativeDestruct() override;
};
