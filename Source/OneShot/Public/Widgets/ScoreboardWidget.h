// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScoreboardWidget.generated.h"

class UScoreboardItemWidget;
class UVerticalBox;

/**
 * 
 */
UCLASS()
class ONESHOT_API UScoreboardWidget : public UUserWidget
{
	GENERATED_BODY()
	
private:
	UPROPERTY(EditAnywhere, Category = "UI")
	TSubclassOf<UScoreboardItemWidget> ScoreboardItemWidgetClass;

	FTimerHandle DrawTimer;

	UFUNCTION()
	void DrawItem();

	UVerticalBox* VerticalBox_Item;
	
protected:
	virtual void NativeConstruct() override;

	virtual void NativeDestruct() override;
	
};
