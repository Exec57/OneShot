// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuPauseWidget.generated.h"

class UButton;

/**
 * 
 */
UCLASS()
class ONESHOT_API UMenuPauseWidget : public UUserWidget
{
	GENERATED_BODY()

private:
	UButton* Button_Resume;

	UFUNCTION()
	void OnButtonClicked_Resume();

	UButton* Button_Options;

	UFUNCTION()
	void OnButtonClicked_Options();

	UButton* Button_MenuMain;

	UFUNCTION()
	void OnButtonClicked_MenuMain();

	UButton* Button_Exit;

	UFUNCTION()
	void OnButtonClicked_Exit();

protected:
	virtual void NativeConstruct() override;

	virtual void NativeDestruct() override;
	
};
