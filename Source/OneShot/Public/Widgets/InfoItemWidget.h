// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InfoItemWidget.generated.h"

class UTextBlock;
class ASPlayerState;

/**
 * 
 */
UCLASS()
class ONESHOT_API UInfoItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
private:
	UTextBlock* TextBlock_One;

	UTextBlock* TextBlock_Two;

	UTextBlock* TextBlock_Three;

	FTimerHandle DestroyTimer;

	UFUNCTION()
	void Destroy();
	
protected:
	virtual void NativeConstruct() override;

public:
	void SetTextOneInfo(const FString& Text, FColor Color = FColor::White);

	void SetTextTwoInfo(const FString& Text, FColor Color = FColor::White);

	void SetTextThreeInfo(const FString& Text, FColor Color = FColor::White);
};
