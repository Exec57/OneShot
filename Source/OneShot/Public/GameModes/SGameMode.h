// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGameMode.generated.h"

class ASPlayerController;
class ABaseCharacter;
class ABotController;

/**
 * 
 */
UCLASS()
class ONESHOT_API ASGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASGameMode();

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Logout(AController* Exiting) override;

	virtual void StartPlay() override;

private:
	int NumBot;

	void SpawnBotController();

	UPROPERTY(EditDefaultsOnly, Category = "Bot")
	TSubclassOf<ABotController> BotControllerClass;
};
