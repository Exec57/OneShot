// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Blastdoor.generated.h"

class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class ONESHOT_API ABlastdoor : public AActor
{
	GENERATED_BODY()
	
public:	
	ABlastdoor();

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

public:	
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* BlastdoorBorderMeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* BlastdoorMeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UBoxComponent* BoxComp;

	FTimerHandle CloseTimer;

	void Open();

	UFUNCTION()
	void Close();

	float ZLocation;

	float TargetZLocation;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundBase* Sound;

	int32 Count;

	bool bOpen;
};
