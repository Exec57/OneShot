// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BotController.generated.h"

class ABotCharacter;
class UBehaviorTreeComponent;
class UBlackboardComponent;

/**
 * 
 */
UCLASS()
class ONESHOT_API ABotController : public AAIController
{
	GENERATED_BODY()
	
private:
	ABotController();

	UPROPERTY(EditDefaultsOnly, Category = "Bot")
	TSubclassOf<ABotCharacter> BotCharacterClass;

	UBehaviorTreeComponent* BehaviorComp;

	UBlackboardComponent* BlackboardComp;

public:
	virtual void Tick(float DeltaTime) override;

	void SpawnBot();

	void OnDeath();
	
	FTimerHandle SpawnTimer;

	virtual void Possess(APawn* InPawn) override;

	virtual void UnPossess() override;

	void SetBlackboardPlayer(APawn* Player);

protected:
	virtual void BeginPlay() override;
};
