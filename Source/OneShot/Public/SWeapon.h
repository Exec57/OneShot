// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SWeapon.generated.h"

class UParticleSystem;
class USoundBase;
class ABaseCharacter;

USTRUCT()
struct FHitScanTrace
{
	GENERATED_BODY()

public:
	UPROPERTY()
	TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY()
	FVector_NetQuantizeNormal TraceEnd;

	UPROPERTY()
	FRotator TraceRotation;

private:
	UPROPERTY()
	uint8 EnsureReplicationByte;

public:
	void EnsureReplication()
	{
		EnsureReplicationByte++;
	}
};

UCLASS()
class ONESHOT_API ASWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	ASWeapon();

	void StartFire();

	void StopFire();

private:
	FName HeadSocketName;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	UParticleSystem* ImpactEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	UParticleSystem* BloodEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	UParticleSystem* TrailEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	float FireRate;

	void Fire();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire(FVector TraceEnd, FHitResult Hit);

	FTimerHandle FireTimer;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	bool bRepeat;

	float LastFireTime;

	UPROPERTY(ReplicatedUsing=OnRep_HitScanTrace)
	FHitScanTrace HitScanTrace;

	UFUNCTION()
	void OnRep_HitScanTrace();

	void PlayTraceEffect(FVector Start, FVector End);

	void PlayImpactEffect(EPhysicalSurface SurfaceType, FVector ImpactPoint, FRotator TraceRotation);

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundBase* FireSound;

	ABaseCharacter* Character;

	bool bIsABot;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundBase* ReloadCompleteSound;

	FTimerHandle FireRateTimer;

	UFUNCTION()
	void PlayReloadCompleteSound();

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundBase* CantFireSound;

protected:
	virtual void BeginPlay() override;
};
